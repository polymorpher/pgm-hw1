/**
 * Created by polymorpher on 2/17/15.
 */

import io.Source
import java.io.File
import collection.mutable

object App {
  val count = Array.fill(12)(0)
  var totalNumSamples = 0;
  //p(0...11) = p(3|0)p(8|1,3,4,5)p(1|0)p(9|1,2,4)p(7|2)p(10|2,6)p(11|1,2,4,6)
  //            p(0)p(4)p(5)p(2)p(6)

  val relations = Array((3, Seq(0)), (8, Seq(1, 3, 4, 5)),
    (1, Seq(0)), (9, Seq(1, 2, 4)), (7, Seq(2)), (10, Seq(2, 6)),
    (11, Seq(1, 2, 4, 6)))

  val singletons = Array(0, 4, 5, 2, 6)

  val model = Array.fill(4096)(0.0)

  val relationParentCounts = mutable.Map[(Int, Seq[Int], Seq[Boolean]), Int]()

  val relationTrueCounts = relationParentCounts.clone

  def buildRelationCounts(): Unit = {
    for (r <- relations) {
      0 to Math.pow(2, r._2.length).toInt - 1 foreach { i =>
        val ar = Array.fill(r._2.length)(false)
        var it = i;
        var ind = 0;
        while (it > 0) {
          ar(ind) = if ((it & 0x01) == 0x01) true else false
          ind += 1
          it = it >> 1
        }
        relationParentCounts += ((r._1, r._2, ar.toSeq) -> 0)
        relationTrueCounts += ((r._1, r._2, ar.toSeq) -> 0)
      }
    }
  }

  buildRelationCounts;


  def computeProb(obs: Int): Double = {
    val buf = Array.fill(12)(false)
    var v = obs
    var ind = 0;
    while (v > 0 && ind <= 11) {
      buf(ind) = if ((v & 0x01) == 0x01) true else false
      v = v >> 1
      ind += 1
    }
    var p = 1.0
    for (r <- relations) {
      val child = r._1
      val parents = r._2
      val pc = relationTrueCounts((child, parents, parents.map(e => buf(e)))).toDouble
      val pp = relationParentCounts((child, parents, parents.map(e => buf(e)))).toDouble
      if (buf(child)) {
        p = p * (pc / pp)
      } else {
        p = p * ((pp - pc) / pp)
      }
    }
    for (s <- singletons) {
      if (buf(s)) {
        p *= (count(s).toDouble / totalNumSamples.toDouble)
      } else {
        p *= (1 - count(s).toDouble / totalNumSamples.toDouble)
      }
    }
    if(p.isNaN || p.isInfinity) 0 else p
  }

  //L1 distance
  def computeDistance(target: Array[Double]): Double = {
    var dis = 0.0
    0 to 4095 foreach { i =>
      dis += Math.abs(model(i) - target(i))
    }
    dis
  }

  def computeKLD(source: Array[Double], target: Array[Double]): Double = {
    var kld = 0.0
    0 to 4095 foreach { i =>
      if(source(i)!=0 && target(i)!=0) {
        kld += (source(i) * Math.log(source(i) / target(i)))
      }
//      println(s"$i ${source(i) * Math.log(source(i) / target(i))} source=${source(i)} target=${target(i)}")
    }
    kld
  }

  def computeJSD(target: Array[Double]): Double = {
    (computeKLD(model, target) + computeKLD(target, model)) / 2.0
  }

  def query(query: (Seq[Int], Seq[Boolean]), condition: (Seq[Int], Seq[Boolean]), m: Array[Double] = model): Double = {
    var queryValue = 0
    var queryMask = 0
    0 to query._1.length - 1 foreach { i =>
      if (query._2(i)) {
        queryValue += Math.pow(2, query._1(i)).toInt
      }
      queryMask += Math.pow(2, query._1(i)).toInt
    }
    var conditionValue = 0
    var conditionMask = 0
    0 to condition._1.length - 1 foreach { i =>
      if (condition._2(i)) {
        conditionValue += Math.pow(2, condition._1(i)).toInt
      }
      conditionMask += Math.pow(2, condition._1(i)).toInt
    }

    var marginalConditionalProb = 0.0
    var jointProb = 0.0
    val jointValue = conditionValue + queryValue
    val jointMask = conditionMask + queryMask
    0 to 4095 foreach { i =>
      if ((i & conditionMask) == conditionValue) {
        marginalConditionalProb += m(i)
      }

      if ((i & jointMask) == jointValue) {
        jointProb += m(i)
      }
    }
    jointProb / marginalConditionalProb
  }

  def main(args: Array[String]) {
    val src = Source.fromInputStream(getClass.getResourceAsStream("/dataset.dat"))
    val buf = Array.fill(12)(false)
    src.getLines().foreach { line =>
      var v = line.toInt
      var ind = 0;
      while (ind <= 11) {
        buf(ind) = if ((v & 0x01) == 0x01) true else false
        v = v >> 1
        ind += 1
      }
      for (r <- relations) {
        val child = r._1
        val parents = r._2
        if (buf(child)) {
          relationTrueCounts((child, parents, parents.map(e => buf(e)))) += 1
        }
        relationParentCounts((child, parents, parents.map(e => buf(e)))) += 1
      }
      0 to 11 foreach { i => if (buf(i)) count(i) += 1}
      totalNumSamples += 1
      0 to 11 foreach { i => buf(i) = false}
    }
    for (r <- relationParentCounts) {
      println(s"${r._1}: Total ${r._2} true: ${relationTrueCounts(r._1)} ratio=${relationTrueCounts(r._1) / r._2.toDouble}")
    }
    for (s <- singletons) {
      println(s"Singleton $s true=${count(s)} ratio=${count(s).toDouble / totalNumSamples.toDouble}")
    }
    //        println(relationTrueCounts)
    var modelProbSum = 0.0
    0 to 4095 foreach { i =>
      val p = computeProb(i)
      model(i) = p
      modelProbSum += p
      //      println(s"$i $p")
    }
    println(s"modelProbSum=$modelProbSum")
    val target = Array.fill(4096)(0.0)
    Source.fromInputStream(getClass.getResourceAsStream("/joint.dat")).getLines().foreach { line =>
      var parts = line.split("\t")
      val id = parts(0).toInt
      val prob = parts(1).toDouble
      target(id) = prob
//      println(s"target $id = ${target(id)}")
    }

    println(s"L1 Distance=${computeDistance(target)} Jensen-Shannon Distance=${computeJSD(target)}")
    println("Queries (c=condition q=query):")
    val c1 = (Seq(8, 11), Seq(true, true))
    val q1 = (Seq(1), Seq(true))
    println(s"c1:$c1 q1:$q1 probEstimated:${query(q1, c1)} probReal:${query(q1, c1, target)}")
    val c2 = (Seq(4), Seq(true))
    val q2 = (Seq(7, 8, 9, 10, 11), Seq(true, true, true, true, true))
    println(s"c2:$c2 q2:$q2 probEstimated:${query(q2, c2)} probReal:${query(q2, c2, target)}")
    val c21 = (Seq(4), Seq(true))
    val q21 = (Seq(7), Seq(true))
    println(s"c21:$c21 q2:$q21 probEstimated:${query(q21, c21)} probReal:${query(q21, c21, target)}")
    val c22 = (Seq(4), Seq(true))
    val q22 = (Seq(8), Seq(true))
    println(s"c22:$c22 q22:$q22 probEstimated:${query(q22, c22)} probReal:${query(q22, c22, target)}")
    val c23 = (Seq(4), Seq(true))
    val q23 = (Seq(9), Seq(true))
    println(s"c23:$c23 q2:$q23 probEstimated:${query(q23, c23)} probReal:${query(q23, c23, target)}")
    val c24 = (Seq(4), Seq(true))
    val q24 = (Seq(10), Seq(true))
    println(s"c24:$c24 q24:$q24 probEstimated:${query(q24, c24)} probReal:${query(q24, c24, target)}")
    val c25 = (Seq(4), Seq(true))
    val q25 = (Seq(11), Seq(true))
    println(s"c25:$c25 q25:$q25 prob:${query(q25, c25)} probReal:${query(q25, c25, target)}")
    val c3 = (Seq(0), Seq(true))
    val q3 = (Seq(10), Seq(true))
    println(s"c3:$c3 q3:$q3 probEstimated:${query(q3, c3)} probReal:${query(q3, c3, target)}")

    var LL=0.0
    Source.fromInputStream(getClass.getResourceAsStream("/dataset.dat")).getLines().foreach{line=>
      var v=line.toInt
      LL+= Math.log(target(v))
    }
    println(s"LL target=$LL")
  }
}
