name := "pgm-hw1"

version := "1.0"

scalaVersion := "2.11.5"

libraryDependencies ++= Seq(
  "org.scalanlp" %% "breeze" % "0.8.1",
  "org.scalanlp" %% "breeze-natives" % "0.8.1",
  "org.scalanlp" %% "breeze-viz" % "0.9",
  "ch.qos.logback" % "logback-classic" % "1.1.2"
)

assemblyJarName in assembly := "hw1.jar"

mainClass in assembly := Some("App")